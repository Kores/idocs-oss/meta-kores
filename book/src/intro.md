# Introduction

## Kores

Kores groups a collection of frameworks and libraries, which aims to improve the software development process.

## idocs.xyz

Currently, the main project of Kores is the [idocs.xyz](https://idocs.xyz) documentation building and hosting platform,
and as a consequence, other projects like [atevist](https://gitlab.com/Kores/Atevist) is being developed to fullfil the platform
needs.

## JVM Runtime Code Generation

Also, Kores provides [KoresFramework](https://gitlab.com/Kores/Kores), a JVM bytecode and Java source generator,
which helps with code generation for the JVM platform and meta-programming.

It is like the well known [bytebuddy](https://bytebuddy.net/#/) and [cglib](https://github.com/cglib/cglib), but lower-level,
with higher-level libraries like [KoresProxy](https://gitlab.com/Kores/KoresProxy) for better Java proxies and [EventSys](https://gitlab.com/Kores/EventSys) for flexible events with mixin-like features, and [AdapterHelper](https://gitlab.com/Kores/AdapterHelper) for Adapter-pattern generation.

KoresFramework is an abstraction over [ObjectWeb ASM](https://asm.ow2.io/), providing an AST-like structure to work with Java classes and methods, as well as generating Java Source Code from the AST-like structure, for Compile-Time Processors (like Annotation Processors).